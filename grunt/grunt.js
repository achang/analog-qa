module.exports = function(grunt) {

  grunt.initConfig({
      json_server: {
            options: {
                port: 13337,
                hostname: '0.0.0.0',
                db: 'acceptance-tests/src/test/resources/mocks/films-mock.json'
            }
        },

        // Run some tasks in parallel to speed up build process
        concurrent: {
            server: {
                tasks: [
                    'json_server',
                    'watch'
                ],
                options: {
                    logConcurrentOutput: true
                }
            }
        }
  });

    grunt.task.run([
        'connect:livereload',
        'concurrent:server'
    ]);

        // register tasks
        grunt.registerTask('run', ['concurrent']);


  grunt.loadNpmTasks('grunt-json-server');
};