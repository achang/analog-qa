/**
  * Created by Andy on 10/18/2017.
  */

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class BasicAnalogQASimulation extends Simulation {
  val httpConf = http.baseURL("http://localhost:3000")

  
  val getFilmsScenario = scenario("Get Films")
    .exec(http("request_1")
      .get("/films"))
    //.pause(5)
  setUp(
    getFilmsScenario.inject(atOnceUsers(10))
  ).protocols(httpConf)
}