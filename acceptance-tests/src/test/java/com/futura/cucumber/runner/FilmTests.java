package com.futura.cucumber.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by Andy on 9/26/2017.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = {"com.futura.cucumber"},
        features = {"src/test/resources/cucumber/features"},
        format = {"pretty", "html:target/site/cucumber-pretty", "json:target/cucumber.json"},
        tags = {"@film", "~@wip"})

public class FilmTests {
}
