package com.futura.cucumber.dataprovider;

import com.futura.domain.FilmDTO.FilmDTO;
import com.futura.domain.FilmDTO.Stockist;
import java.util.Arrays;

/**
 * Created by Andy on 10/26/2017.
 */
public class FilmDTOGenerator {
    public static FilmDTO getValidFilmDTO() {

        return new FilmDTO.FilmDTOBuilder("Generated Film")
                .withManufacturer("AnalogInc")
                .withFormat("120")
                .withISO(100)
                .withStockists(Arrays.asList(
                        new Stockist.StockistBuilder("Andys camera store")
                                .withRegion("London")
                                .build(),
                        new Stockist.StockistBuilder("Zoing")
                                .withRegion("Brighton")
                                .build()))
                .build();
    }
}
