package com.futura.cucumber.hooks;

import com.futura.FilmAPI;
import com.futura.cucumber.domainshared.FilmShared;
import com.futura.cucumber.domainshared.ResponseShared;
import com.futura.domain.FilmDTO.Stockist;
import com.futura.helper.RequestHelper;
import com.futura.helper.URLGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import cucumber.api.java.After;
import io.restassured.http.Method;

/**
 * Created by Andy on 10/27/2017.
 */
public class TearDownHooks {

    private FilmAPI filmAPI;
    public TearDownHooks() {
        filmAPI = new FilmAPI();
    }

    @After(value = "@DeleteFilm", order = 5)
    public void deleteFilmIfPresent() throws Exception {
        if (FilmShared.film.getId() != null) {
            if (filmAPI.isFilmPresentById(URLGenerator.getURL(), FilmShared.film.getId())) {
                RequestHelper.run(
                        filmAPI.getFilmById(URLGenerator.getURL(),
                                FilmShared.film.getId()),
                        Method.DELETE);
            }
        }
    }

    @After(value = "@DeleteUpdatedStockists", order = 6)
    public void deleteUpdatedStockists() throws JsonProcessingException{
        if(FilmShared.additionalStockists != null) {

            // Remove suppliers from Film
            for(Stockist stockist : FilmShared.additionalStockists) {
                FilmShared.film.getStockists().remove(stockist);
            }

            // post updated film
            ResponseShared.response = RequestHelper.run(
                    filmAPI.addFilm(URLGenerator.getUrlWithFilmId(URLGenerator.getURL(), FilmShared.film.getId()),
                            FilmShared.film),
                    Method.PUT);
        }
    }
}
