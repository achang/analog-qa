package com.futura.cucumber.hooks;

import com.futura.FilmAPI;
import com.futura.cucumber.dataprovider.FilmDTOGenerator;
import com.futura.cucumber.domainshared.BuiltRequestShared;
import com.futura.cucumber.domainshared.FilmShared;
import com.futura.cucumber.domainshared.ResponseShared;
import com.futura.helper.RequestHelper;
import com.futura.helper.URLGenerator;
import com.futura.tfc.selenium.helper.LoggerHelper;
import com.futura.tfc.selenium.helper.PropertiesHelper;
import cucumber.api.java.Before;
import io.restassured.http.Method;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andy on 7/29/2017.
 */
public class SetupHooks {

    private BuiltRequestShared builtRequestShared = null;
    private FilmShared filmShared = null;
    private ResponseShared responseShared = null;
    private FilmAPI filmAPI;

    public SetupHooks(BuiltRequestShared builtRequestShared, FilmShared filmShared, ResponseShared responseShared) {
        this.builtRequestShared = builtRequestShared;
        this.filmShared = filmShared;
        this.responseShared = responseShared;
        filmAPI = new FilmAPI();
    }

    private Logger logger = LoggerHelper.getLogger(SetupHooks.class);

    /**
     * Uses envintest key in environmentInTest.properties used to determine which environment parameters to load
     */
    @Before(order = 2)
    public void loadParameterConfiguration() {

        PropertiesHelper.setPropertiesFileForEnvironment(getEnvironmentInTestProperty());

        PropertiesHelper.setPropertiesFileForEnvironment(
                getPropertiesFiles()
                        .toArray(new String[(getPropertiesFiles()
                                .size())]));
    }

    @Before(order = 3)
    public void setFilmServiceEndpoint() {
        URLGenerator.setURL(PropertiesHelper.getStringProperty("server")
                + PropertiesHelper.getStringProperty("films.endpoint"));

    }

    @Before(value = "@ResetSharedDomainObjects", order = 4)
    public void clearSharedDomainObjects() {
        builtRequestShared.requestSpecBuilder = null;
        filmShared.filmList = null;
        filmShared.film = null;
        responseShared.response = null;
    }

    @Before(value = "@CreateFilm", order = 5)
    public void createANewFilm() throws Exception {

        // Generate film data
        FilmShared.film = FilmDTOGenerator.getValidFilmDTO();

        // Create a new film
        ResponseShared.response = RequestHelper.run(
                filmAPI.addFilm(URLGenerator.getURL(),
                        FilmShared.film),
                Method.POST);

        // Extract the generated film id and set it in the shared film domain object
        FilmShared.film.withId(filmAPI.getFilmIdFromResponse(
                ResponseShared.response));
    }

    private String getEnvironmentInTestProperty() {
        String environmentInTestProperties = "properties/environmentintest.properties";
        return environmentInTestProperties;
    }

    /**
     * Load the base application properties and then the relevant properties depending on the
     * envintest value
     *
     * @return
     */
    private List<String> getPropertiesFiles() {
        List<String> propertyFiles = new ArrayList<>();

        String environmentInTest = PropertiesHelper.getStringProperty("envintest");

        propertyFiles.add("properties/base.properties");

        switch (environmentInTest) {
            case "local": {
                propertyFiles.add("properties/"+environmentInTest+".properties");
                break;
            }
            default: {
                logger.error("Unable to find environment in test configuration");
            }
        }

        return propertyFiles;
    }

}
