package com.futura.cucumber.domainshared;

import com.futura.domain.FilmDTO.FilmDTO;
import com.futura.domain.FilmDTO.Stockist;

import java.util.List;

/**
 * Created by Andy on 10/12/2017.
 */
public class FilmShared {
    public static List<FilmDTO> filmList;
    public static FilmDTO film;
    public static List<Stockist> additionalStockists;
}
