package com.futura.cucumber.stepdef;

import com.futura.FilmAPI;
import com.futura.cucumber.domainshared.BuiltRequestShared;
import com.futura.cucumber.domainshared.FilmShared;
import com.futura.cucumber.domainshared.ResponseShared;
import com.futura.domain.FilmDTO.FilmDTO;
import com.futura.domain.FilmDTO.Stockist;
import com.futura.helper.RequestHelper;
import com.futura.helper.ResponseHelper;
import com.futura.helper.URLGenerator;
import com.futura.tfc.selenium.helper.PropertiesHelper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import io.restassured.http.Method;

import java.util.List;
import java.util.Map;

/**
 * Created by Andy on 7/29/2017.
 */
public class FilmStepdefs {

    //query film
    //http://localhost:3000/films?filmName=cinestill&iso=800

    private FilmAPI filmAPI = new FilmAPI();

    @Given("^I want to view films with the following filters:$")
    public void setupCustomFilmQuery(Map<String, String> parameters) throws Throwable {
        BuiltRequestShared.requestSpecBuilder = filmAPI.getFilteredFilms(URLGenerator.getURL(), parameters);
    }

    @Given("^I want to (?:view details for|remove) film id (\\d+)$")
    public void setUpRequestForFilmId(Integer filmId) throws Throwable {
        BuiltRequestShared.requestSpecBuilder = filmAPI.getFilmById(URLGenerator.getURL(), filmId);
    }

    @Given("^I want to view all film stock$")
    public void viewAllFilmStock() throws Throwable {
        BuiltRequestShared.requestSpecBuilder = filmAPI.getAllFilms(URLGenerator.getURL());
    }

    @Given("^the following new film has been added:$")
    public void addNewFilm(List<FilmDTO> films) throws Throwable {
        FilmShared.film = films.get(0);
    }

    @And("^made available in the following stockists:$")
    public void madeAvailableInTheFollowingStockists(List<Stockist> stockists) throws Throwable {
        FilmShared.film.addStockists(stockists);
        BuiltRequestShared.requestSpecBuilder = filmAPI.addFilm(URLGenerator.getURL(), FilmShared.film);
    }

    @Given("^I want to remove a newly created film$")
    public void removeExitingFilm() throws Throwable {
        setUpRequestForFilmId(FilmShared.film.getId());
    }

    @And("^search for the deleted film$")
    public void searchForTheDeletedFilm() throws Throwable {
//        filmAPI.waitForServiceToBeAvailable(
//                PropertiesHelper.getIntegerProperty("pollInMS"),
//                PropertiesHelper.getIntegerProperty("maxTimeoutSec"));

        BuiltRequestShared.requestSpecBuilder = filmAPI.getFilmById(URLGenerator.getURL(), FilmShared.film.getId());
        ResponseShared.response = RequestHelper.run(BuiltRequestShared.requestSpecBuilder, Method.GET);
    }

    @And("^retrieve the following parameters:$")
    public void retrieveParameters(List<String> parametersToRetrieve) throws Throwable {
        for (String parameter : parametersToRetrieve) {
            ResponseShared.extractParameters.put(parameter,
                    ResponseHelper.getValuesFromResponse(
                            ResponseShared.response,
                            parameter));
        }
    }

    @Given("^I want add (?:a new stockists|new stockists) to film id (\\d+) :$")
    public void addANewStockistsToFilmId(int filmId, List<Stockist> stockistsToAdd) throws Throwable {
        BuiltRequestShared.requestSpecBuilder = filmAPI.addSupplierToFilm(URLGenerator.getURL(), stockistsToAdd, filmId);
        FilmShared.additionalStockists = stockistsToAdd;
    }
}
