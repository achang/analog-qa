package com.futura.cucumber.stepdef;

import com.futura.FilmAPI;
import com.futura.cucumber.domainshared.BuiltRequestShared;
import com.futura.cucumber.domainshared.FilmShared;
import com.futura.cucumber.domainshared.ResponseShared;
import com.futura.domain.FilmDTO.FilmDTO;
import com.futura.domain.FilmDTO.Stockist;
import com.futura.helper.RequestHelper;
import com.futura.helper.URLGenerator;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.Method;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by Andy on 7/29/2017.
 */
public class CommonStepdefs {

    private FilmAPI filmAPI = new FilmAPI();

    @When("^I perform the (GET|POST|DELETE|PUT) query$")
    public void performTheQuery(String httpMethod) throws Throwable {
        ResponseShared.response = RequestHelper.run(BuiltRequestShared.requestSpecBuilder, Method.valueOf(httpMethod));
        ResponseShared.response.prettyPeek();
    }

    @Then("^the response status code will be (\\d+)$")
    public void theResponseStatusCodeWillBe(int expectedStatusCode) throws Throwable {
        assertThat("Response status code will be ",
                ResponseShared.response.getStatusCode(),
                equalTo(expectedStatusCode));
    }

    @Then("^the following film details are returned:$")
    public void theFollowingFilmDetailsAreReturned(List<FilmDTO> expectedFilms) throws Throwable {
        List<FilmDTO> actualFilms = filmAPI.mapResponseToFilms(ResponseShared.response);

        for (FilmDTO expectedFilm : expectedFilms) {
            assertThat("Verify Film details are correct",
                    actualFilms,
                    hasItem(expectedFilm));
        }
    }

    @And("^the new film will be successfully added$")
    public void theNewFilmWillBeSuccessfullyAdded() throws Throwable {

        // Set reference film with the generated id
        FilmShared.film.withId(filmAPI.getFilmIdFromResponse(ResponseShared.response));

        // Retrieve the Film using the generated id
        BuiltRequestShared.requestSpecBuilder = filmAPI.getFilmById(URLGenerator.getURL(), FilmShared.film.getId());
        performTheQuery("GET");

        assertThat("Film has been successfully added",
                FilmShared.film,
                equalTo(filmAPI.mapResponseToFilm(ResponseShared.response)));
    }

    @Then("^the \"([^\"]*)\" will contain:$")
    public void verifyValuesFromRetrievedParameters(String parameter, List<String> valuesToCheck) throws Throwable {
        List<String> actualValues = ResponseShared.extractParameters.get(parameter);

        for (String valueToCheck : valuesToCheck) {
            assertThat("Values are present in extracted parameters",
                    actualValues,
                    hasItem(valueToCheck));
        }
    }

    @Then("^the (?:new stockists|new stockist) will be successfully added$")
    public void theNewStockistsWillBeSuccessfullyAdded() throws Throwable {

       FilmShared.film = filmAPI.mapResponseToFilm(ResponseShared.response);

        for (Stockist stockist : FilmShared.additionalStockists) {
            assertThat("Stockists has been added successfully",
                    FilmShared.film.getStockists(),
                    hasItem(stockist));
        }
    }
}
