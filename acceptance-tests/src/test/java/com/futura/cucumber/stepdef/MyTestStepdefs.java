package com.futura.cucumber.stepdef;

import cucumber.api.java.en.Given;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

import static io.restassured.RestAssured.given;

/**
 * Created by Andy on 10/19/2017.
 */
public class MyTestStepdefs {
    @Given("^I am making a test call$")
    public void iAmMakingATestCall() throws Throwable {

        try {
            Response response = given()
                    .contentType(ContentType.JSON)
                    .baseUri("http://localhost:3000/films")
                    .get();

            RequestSpecification requestSpecification = given()
                    .contentType(ContentType.JSON)
                    .baseUri("http://localhost:3000/films");

            requestSpecification.get();

            Integer[] numberArray = {10, 1, 23, 4, 63, 34};

            System.out.println("default array");
            printArray(numberArray);

            Arrays.sort(numberArray);
            System.out.println("default sorted array");
            printArray(numberArray);

            LinkedList<Integer> numberList = new LinkedList();
            numberList.add(1);
            numberList.add(32);
            numberList.add(23);
            numberList.add(65);
            numberList.add(34);

            Comparator comparator = Comparator.reverseOrder();

            Collections.sort(numberList, comparator);
            System.out.println("Collection sorted array");
            for (Integer i : numberList) {
                System.out.println(i);
            }
        }catch(Exception ex) {}
    }

    public void printArray(Integer[] numberArrayToPrint) {
        for (int i = 0; i < numberArrayToPrint.length; i++) {
            System.out.println(numberArrayToPrint[i]);
        }
    }
}
