@film
Feature: View film
  As a film stockist
  I want to query the available film
  So that I can purchase stock

  @ResetSharedDomainObjects
  Scenario: View all films
    Given I want to view all film stock
    When I perform the GET query
    Then the following film details are returned:
      | id | filmName  | manufacturer | format | iso |
      | 1  | portra    | kodak        | 35mm   | 400 |
      | 2  | portra    | kodak        | 35mm   | 800 |
      | 4  | cinestill | cinestill    | 35mm   | 800 |

  @ResetSharedDomainObjects
  Scenario: Verify Custom filters
    Given I want to view films with the following filters:
      | filmName | cinestill |
      | iso      | 800       |
    When I perform the GET query
    Then the response status code will be 200

  @ResetSharedDomainObjects
  Scenario: Verify list of manufactures and films
    Given I want to view all film stock
    When I perform the GET query
    And retrieve the following parameters:
      | filmName     |
      | manufacturer |
    Then the "filmName" will contain:
      | portra    |
      | cinestill |
    And the "manufacturer" will contain:
      | kodak     |
      | cinestill |

  @ResetSharedDomainObjects
  Scenario: Verify details for a film
    Given I want to view details for film id 3
    When I perform the GET query
    Then the following film details are returned:
      | id | filmName | manufacturer | format | iso |
      | 3  | velvia   | kodak        | 35mm   | 50  |
    And the response status code will be 200

  @ResetSharedDomainObjects @DeleteFilm
  Scenario: Add a new film
    Given the following new film has been added:
      | filmName   | manufacturer | format | iso |
      | velvia new | kodak        | 35mm   | 50  |
    And made available in the following stockists:
      | name    | region   |
      | jessops | london   |
      | jessops | brighton |
    When I perform the POST query
    Then the response status code will be 201
    And the new film will be successfully added

  @ResetSharedDomainObjects
  Scenario: Delete a non-existent film
    Given I want to remove film id 1000
    When I perform the DELETE query
    Then the response status code will be 404

  @ResetSharedDomainObjects @DeleteUpdatedStockists
  Scenario: Update an existing Film with a new stockists
    Given I want add new stockists to film id 3 :
      | name              | region              |
      | New Supplier Test | New Supplier Region |
      | New Supplier Test2 | New Supplier Region2 |
    When I perform the PUT query
    Then the new stockists will be successfully added

  @CreateFilm @ResetSharedDomainObjects
  Scenario: Delete an existing film
    Given I want to remove a newly created film
    When I perform the DELETE query
    And search for the deleted film
    Then the response status code will be 404

  @wip
  Scenario: Update stock for a film
    Given "velvia" has been purchased from "calumet" in "london"
    When I perform the query
    Then the response status code will be 200

  @wip
  Scenario: This is a quick test
    Given I am making a test call


