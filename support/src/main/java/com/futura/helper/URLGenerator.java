package com.futura.helper;

/**
 * Created by Andy on 8/2/2017.
 */
public class URLGenerator {

    private static String internalUrl;

    public static void setURL(String url) {
        internalUrl = url;
    }

    public static String getURL() {
        return internalUrl;
    }

    public static String getUrlWithFilmId(String url, Integer filmId) {
        return  url + "/" + filmId.toString();
    }
}
