package com.futura.helper;

import com.futura.tfc.api.helper.WaitHelper;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.Method;
import io.restassured.specification.ResponseSpecification;

/**
 * Created by Andy on 11/22/2017.
 */
public class SynchronisationHelper {

    public static void waitForRequestToReturnStatusCode(RequestSpecBuilder requestSpecBuilder, Method method, Integer pollMS,
                                                       Integer maxTimeoutSeconds, Integer expectedStatusCode) {
        ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder();
        responseSpecBuilder.expectStatusCode(expectedStatusCode);
        waitForRequest(requestSpecBuilder, method, pollMS, maxTimeoutSeconds, responseSpecBuilder.build());
    }

    private static void waitForRequest(RequestSpecBuilder requestSpecBuilder, Method method, Integer pollMS,
                                      Integer maxTimeoutSeconds, ResponseSpecification responseSpecification) {
        WaitHelper.waitForAPIResponse(
                pollMS,
                maxTimeoutSeconds).untilAsserted(() ->
                RequestHelper.run(requestSpecBuilder, method)
                        .then()
                .spec(responseSpecification));
    }
}
