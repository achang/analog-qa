package com.futura.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;

/**
 * Created by Andy on 8/13/2017.
 */
public class DataParser {

    private ObjectMapper objectMapper = new ObjectMapper();
    JsonParser jsonParser = new JsonParser();

    public DataParser() {
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    }

    public JsonNode parseToJsonNode(String jsonString) throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readTree(jsonString);
    }

    public JsonObject mapObjectToJsonObject(Object objectToMap) throws JsonProcessingException {
        return (JsonObject) jsonParser.parse(objectMapper.writeValueAsString(objectToMap));
    }

    public ObjectMapper getMapper(){
        return objectMapper;
    }
}

