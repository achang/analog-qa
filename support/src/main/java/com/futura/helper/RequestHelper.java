package com.futura.helper;

import com.futura.tfc.selenium.helper.LoggerHelper;
import com.google.gson.JsonObject;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.log4j.Logger;

import java.util.Map;

import static io.restassured.RestAssured.given;

/**
 * Created by Andy on 9/21/2017.
 */
public class RequestHelper {

    private static Logger logger = LoggerHelper.getLogger(RequestHelper.class);

    private static RequestSpecification requestSpecification;

    public static RequestSpecBuilder build(String url, Map<String, String> parameters) {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.addQueryParams(parameters);
        requestSpecBuilder.setBaseUri(url);
        return requestSpecBuilder;
    }

    public static RequestSpecBuilder build(String url) {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri(url);
        return requestSpecBuilder;
    }

    public static RequestSpecBuilder build(String url, JsonObject json) {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri(url);
        requestSpecBuilder.setBody(json.toString());
        requestSpecBuilder.addHeader("Content-Type", "application/json");
        return requestSpecBuilder;
    }

    public static Response run(RequestSpecBuilder requestSpecBuilder, Method method) {
        requestSpecification = requestSpecBuilder.build();
        return requestWithRetry(requestSpecification, method);
    }

    public static Response runRequestWithToken(RequestSpecBuilder requestSpecBuilder, String token) {
        requestSpecification = requestSpecBuilder.build();
        requestSpecification.auth().preemptive().oauth2(token);
        return given(requestSpecification).get();
    }

    private static Response requestWithRetry(RequestSpecification requestSpecification, Method method) {
        Response response = null;
        Boolean success = false;

        while(!success) {
            try{
                response = given(requestSpecification).request(method);
                success = true;
            }catch (Exception exception) {
                logger.warn("**** Problem running request retry - " + exception.getLocalizedMessage());
            }
        }

        return response;
    }
}

