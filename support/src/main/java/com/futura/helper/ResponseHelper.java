package com.futura.helper;

import io.restassured.response.Response;

import java.util.List;

/**
 * Created by Andy on 10/27/2017.
 */
public class ResponseHelper {
    public static Object getValueFromResponse(Response response, String queryPath) {
        return response.path(queryPath);
    }

    public static List<String> getValuesFromResponse(Response response, String queryPath) {
        return response.path(queryPath);
    }
}
