//package co.futura.helper;
//
//import io.restassured.http.Method;
//import io.restassured.response.Response;
//import io.restassured.response.ValidatableResponse;
//import io.restassured.specification.RequestSpecification;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.function.Consumer;
//
//import static io.restassured.RestAssured.given;
//import static io.restassured.http.ContentType.JSON;
//import static io.restassured.http.ContentType.XML;
//
///**
// * Created by Andy on 8/1/2017.
// */
//public class RequestHelperTemp {
//
//    // simple get request
//    public static Response getJsonResponse(String url, Object... urlTemplateVars) throws Exception{
//        return getUrl(url, expect().withJsonContent().withStatusCode(200), urlTemplateVars);
//    }
//
//    /**
//     * Method - get
//     * Url - end point non parameterised
//     * with - Request options
//     * validator - response expectation
//     * urlTemplateVars - parameters to go into url
//     * @param url
//     * @param customValidator
//     * @param urlTemplateVars
//     * @return
//     */
//    private static Response getUrl(String url, Consumer<ValidatableResponse> customValidator, Object... urlTemplateVars) throws Exception{
//        return execute(Method.GET.name(), url, with(), customValidator, urlTemplateVars);
//    }
//
//
//    private static Response execute(String method, String url, Consumer<RequestSpecification> customRequestBuilder, Consumer<ValidatableResponse> customValidator, Object... urlTemplateVars) throws Exception {
//        RequestSpecification requestSpecification = given();
//        customRequestBuilder.accept(requestSpecification);
//        ValidatableResponse validatableResponse = requestSpecification
//                .when()
//                .request(method, url, urlTemplateVars)
//                .then();
//        customValidator.accept(validatableResponse);
//        return validatableResponse.extract().response();
//    }
//
//    /**
//     * Automatic Validation to add to response
//     * @return
//     */
//    public static CustomValidatorBuilder expect() {
//        return new CustomValidatorBuilder();
//    }
//
//    /**
//     * Optional properties to add to validator of response
//     */
//    public static class CustomValidatorBuilder implements Consumer<ValidatableResponse> {
//        private List<Consumer<ValidatableResponse>> validators = new ArrayList<>();
//
//        public CustomValidatorBuilder withJsonContent() {
//            return this.and(response -> response.contentType(JSON));
//        }
//
//        public CustomValidatorBuilder withXMLContent() {
//            return this.and(response -> response.contentType(XML));
//        }
//
//
//        public CustomValidatorBuilder withStatusCode(int statusCode) {
//            return this.and(response -> response.statusCode(statusCode));
//        }
//
//        public CustomValidatorBuilder and(Consumer<ValidatableResponse> consumer) {
//            validators.add(consumer);
//            return this;
//        }
//
//        @Override
//        public void accept(ValidatableResponse response) {
//            validators.stream().forEach(validator -> validator.accept(response));
//        }
//    }
//
//
//    /**
//     * Request builder
//     * @return
//     */
//    public static CustomRequestBuilder with() { return new CustomRequestBuilder(); }
//
//    /**
//     * Optional properties on the request on the request e.g. add token, body etc.
//     */
//    public static class CustomRequestBuilder implements Consumer<RequestSpecification> {
//
//        private List<Consumer<RequestSpecification>> builders = new ArrayList<>();
//
//        public CustomRequestBuilder withToken() {
//            return this.and(requestSpecification -> requestSpecification.auth().preemptive().oauth2(TokenGenerator.getToken()));
//        }
//
//        public CustomRequestBuilder withBody(Object body) {
//            return this.and(requestSpecification -> requestSpecification.body(body));
//        }
//
//        public CustomRequestBuilder and(Consumer<RequestSpecification> consumer) {
//            builders.add(consumer);
//            return this;
//        }
//
//        @Override
//        public void accept(RequestSpecification requestSpecification) {
//            builders.stream().forEach(builder -> builder.accept(requestSpecification));
//        }
//    }
//}
