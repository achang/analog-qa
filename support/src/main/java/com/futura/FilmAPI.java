package com.futura;

import com.futura.domain.FilmDTO.Stockist;
import com.futura.helper.*;
import com.futura.domain.FilmDTO.FilmDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.Method;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by Andy on 7/29/2017.
 */
public class FilmAPI {

    private DataParser dataParser;

    public FilmAPI() {
        dataParser = new DataParser();
    }

    public RequestSpecBuilder getFilmById(String url, Integer id) {
        return RequestHelper.build(URLGenerator.getUrlWithFilmId(url, id));
    }

    public Boolean isFilmPresentById(String url, Integer id) {
        Integer statusCode = RequestHelper.run(
                RequestHelper.build(URLGenerator.getUrlWithFilmId(url, id)), Method.GET).getStatusCode();

        if (statusCode == 200) {
            return true;
        } else {
            return false;
        }
    }

    public RequestSpecBuilder getFilteredFilms(String url, Map<String, String> parameters) {
        return RequestHelper.build(url, parameters);
    }

    public RequestSpecBuilder getAllFilms(String url) {
        return RequestHelper.build(url);
    }

    public List<FilmDTO> mapResponseToFilms(Response response) throws IOException {
        return dataParser.getMapper().readValue(response.body().asString(), new TypeReference<List<FilmDTO>>() {
        });
    }

    public FilmDTO mapResponseToFilm(Response response) throws IOException {
        return dataParser.getMapper().readValue(response.body().asString(), new TypeReference<FilmDTO>() {
        });
    }

    public RequestSpecBuilder addFilm(String url, FilmDTO film) throws JsonProcessingException {
        return RequestHelper.build(url, dataParser.mapObjectToJsonObject(film));
    }

    public Integer getFilmIdFromResponse(Response response) {
        return Integer.valueOf(ResponseHelper.getValueFromResponse(response, "id").toString());
    }

    public RequestSpecBuilder addSupplierToFilm(String url, List<Stockist> stockistsToAdd, Integer filmId) throws IOException {

        // Convert the film json into a DTO
        Response response = RequestHelper.run(getFilmById(url, filmId), Method.GET);

        FilmDTO updatedFilm = addStockistToFilmDTO(mapResponseToFilm(response), stockistsToAdd);

        // convert object into JsonNode
        return RequestHelper.build(
                URLGenerator.getUrlWithFilmId(
                        url,
                        filmId),
                dataParser.mapObjectToJsonObject(updatedFilm));
    }

    public FilmDTO addStockistToFilmDTO(FilmDTO film, List<Stockist> stockists) {
        for (Stockist stockist : stockists) {
            film.addStockist(stockist);
        }

        return film;
    }

    public void waitForServiceToBeAvailable(Integer pollMS, Integer maxTimeoutSeconds) {
        SynchronisationHelper.waitForRequestToReturnStatusCode(
                getAllFilms(URLGenerator.getURL()),
                Method.GET,
                pollMS,
                maxTimeoutSeconds, 200);
    }
}