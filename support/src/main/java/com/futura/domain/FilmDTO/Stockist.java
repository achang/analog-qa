package com.futura.domain.FilmDTO;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by Andy on 8/13/2017.
 */
public class Stockist {

    public String name;
    public String region;

    public Stockist() {}

    // Fields to use when generating the hashcode
    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(name)
                .append(region)
                .toHashCode();
    }

    // Fields to examine in comparison
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Stockist)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        Stockist comparisonSupplier = (Stockist) obj;

        return new EqualsBuilder()
                .append(name, comparisonSupplier.name)
                .append(region, comparisonSupplier.region)
                .isEquals();
    }
    public Stockist(StockistBuilder stockistBuilder) {
        this.name = stockistBuilder.name;
        this.region = stockistBuilder.region;
    }

    public String getName() {
        return name;
    }

    public String getRegion() {
        return region;
    }

    public static class StockistBuilder {
        String name;
        String region;

        public StockistBuilder(String name) {
            this.name = name;
        }

        public StockistBuilder withRegion(String region) {
            this.region = region;
            return this;
        }

        public Stockist build() {
            Stockist stockist = new Stockist(this);
            return stockist;
        }
    }

}
