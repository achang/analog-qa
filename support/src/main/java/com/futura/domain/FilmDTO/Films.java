package com.futura.domain.FilmDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Andy on 10/3/2017.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Films {
    public Films() {}

    public List<FilmDTO> films;
}
