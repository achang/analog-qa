package com.futura.domain.FilmDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.List;

/**
 * Created by Andy on 7/30/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FilmDTO {

    public String filmName, manufacturer, format;
    public Integer id, iso;
    public List<Stockist> stockists;

    public FilmDTO() {
    }

    // Fields to use when generating the hashcode
    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id)
                .append(filmName)
                .append(manufacturer)
                .append(format)
                .append(iso)
                .toHashCode();
    }

    // Fields to examine in comparison
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof FilmDTO)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        FilmDTO comparisonFilmDTO = (FilmDTO) obj;

        return new EqualsBuilder()
                .append(id, comparisonFilmDTO.id)
                .append(filmName, comparisonFilmDTO.filmName)
                .append(manufacturer, comparisonFilmDTO.manufacturer)
                .append(format, comparisonFilmDTO.format)
                .append(iso, comparisonFilmDTO.iso)
                .isEquals();
    }

    public FilmDTO addStockists(List<Stockist> stockists) {
        this.stockists = stockists;
        return this;
    }

    public FilmDTO addStockist(Stockist stockist) {
        this.stockists.add(stockist);
        return this;
    }

    public List<Stockist> getStockists() {
        return this.stockists;
    }

    public FilmDTO withId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public FilmDTO(FilmDTOBuilder filmDTOBuilder) {
        filmName = filmDTOBuilder.filmName;
        manufacturer = filmDTOBuilder.manufacturer;
        format = filmDTOBuilder.format;
        iso = filmDTOBuilder.iso;
        stockists = filmDTOBuilder.stockists;
    }

    public static class FilmDTOBuilder {
        public String filmName, manufacturer, format;
        public Integer iso;
        public List<Stockist> stockists;

        public FilmDTOBuilder(String filmName) {
            this.filmName = filmName;
        }

        public FilmDTOBuilder withManufacturer(String manufacturer) {
            this.manufacturer = manufacturer;
            return this;
        }

        public FilmDTOBuilder withFormat(String format) {
            this.format = format;
            return this;
        }

        public FilmDTOBuilder withISO(Integer iso) {
            this.iso = iso;
            return this;
        }

        public FilmDTOBuilder withStockists(List<Stockist> stockists) {
            this.stockists = stockists;
            return this;
        }

        public FilmDTO build() {
            FilmDTO filmDTO = new FilmDTO(this);
            return filmDTO;
        }
    }
}
