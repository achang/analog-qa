Test project which uses JSON Server, Rest assured and cucumber to demonstrate the the creation of acceptance tests.

Notes
- The project has a dependency on Test framework common library.

Setting up mock server
- install json server
- navigate to \acceptance-tests\test\resources\mocks
- Run command $ json-server --watch films-mock.json
